"""
A simple Python module for accessing many of the Windows API functions from Python code.
A more robust and full alternative is the pywin32 extentions: https://github.com/mhammond/pywin32

author: upornikov@gmail.com 
"""

from ctypes import  Structure, Union, wintypes, windll, \
    create_unicode_buffer, cast as type_cast
from ctypes.wintypes import HWND, HANDLE, HRESULT, MAX_PATH, MSG, \
    byref, LPCWSTR, LPWSTR, LPVOID, HLOCAL,\
    WPARAM, LPARAM, DWORD, WORD, SHORT, BYTE, WCHAR, BOOL, UINT
from ctypes import c_char as CHAR, POINTER, pointer, sizeof, c_int
from exceptions import WindowsError
import inspect

#==tools==================================
class AttrDict(dict):
    __doc__ = "dict with attribute access"

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

def get_caller():
    return "%s::%s"%(inspect.stack()[1][1], inspect.stack()[0][3])

#=========================================

#==windefs================================
try:    #workaround to handle dif in python vs. ironpython type conversion
    INVALID_HANDLE_VALUE = HANDLE(-1).value
    STD_INPUT_HANDLE = DWORD(-10).value
    STD_OUTPUT_HANDLE = DWORD(-11).value
    STD_ERROR_HANDLE = DWORD(-12).value
except:
    INVALID_HANDLE_VALUE = 4294967295L
    STD_INPUT_HANDLE = 4294967286L
    STD_OUTPUT_HANDLE = 4294967285L
    STD_ERROR_HANDLE = 4294967284L

PROCESS_TERMINATE = 1
PROCESS_QUERY_INFORMATION = 1024

CREATE_NEW_CONSOLE = 16
CREATE_UNICODE_ENVIRONMENT = 1024
CREATE_DEFAULT_ERROR_MODE = 67108864

STARTF_USESHOWWINDOW = 1
SW_HIDE = 0
SW_SHOWNORMAL = 1
SW_SHOWMINIMIZED = 2
SW_MAXIMIZE = 3
SW_SHOWMAXIMIZED = 3
SW_SHOWNOACTIVATE = 4
SW_SHOW = 5

CSIDL_COMMON_APPDATA = 35
CSIDL_PERSONAL = 5
CSIDL_APPDATA = 26


STILL_ACTIVE = 259

GENERIC_READ = 2147483648L
GENERIC_WRITE = 1073741824L
FILE_SHARE_READ = 1
FILE_SHARE_WRITE = 2
FILE_SHARE_DELETE = 4
CREATE_NEW = 1
CREATE_ALWAYS = 2
OPEN_EXISTING = 3
OPEN_ALWAYS = 4
TRUNCATE_EXISTING = 5

ERROR_INVALID_FUNCTION = 1
ERROR_ACCESS_DENIED = 5
ERROR_NOT_SUPPORTED = 50
ERROR_DEVICE_NO_RESOURCES = 322

FORMAT_MESSAGE_FROM_SYSTEM = 4096
FORMAT_MESSAGE_ALLOCATE_BUFFER = 256
FORMAT_MESSAGE_IGNORE_INSERTS = 512

CTRL_C_EVENT = 0
CTRL_BREAK_EVENT = 1
KEY_EVENT = 1

WM_USER = 1024

THREAD_SUSPEND_RESUME = 2

SYMBOLIC_LINK_FLAG_DIRECTORY = 1

#=============================

#==Win32 data structures============================
#do not split the _field_ lines: ironpython does not accept that
class STRUCT(Structure):
    def __len__(self):
        return ctypes.sizeof(self)
    def __iter__(self):
        for attr in self._fields_:
            yield attr
    def __getitem__(self, key):
        return self.__getattribute__(key)
    def __setitem__(self, key, value):
        return self.__setattr__(key, value)
    def to_dict(self):
        return AttrDict((k, self[k].to_dict() if hasattr(v, 'to_dict') else self[k]) for k, v in self)
    def copy_from(self, src):
        assert(type(self) == type(src))
        for k, v in self:
            if hasattr(v, 'copy_from'):
                self[k].copy_from(src[k])
            else:
                self[k] = src[k]

class UNION(Union):
    def __len__(self):
        return ctypes.sizeof(self)
    def __iter__(self):
        for attr in self._fields_:
            yield attr
    def __getitem__(self, key):
        return self.__getattribute__(key)
    def __setitem__(self, key, value):
        return self.__setattr__(key, value)
    def to_dict(self):
        """Careful: mutable (union) data is converted to immutable dict fields"""
        return AttrDict((k, self[k].to_dict() if hasattr(v, 'to_dict') else self[k]) for k, v in self)
    def copy_from(self, src):
        assert(type(self) == type(src))
        for k, v in self:
            if hasattr(v, 'copy_from'):
                self[k].copy_from(src[k])
            else:
                self[k] = src[k]

class SECURITY_ATTRIBUTES(STRUCT):
    _fields_ = [("Length", DWORD), ("SecDescriptor", LPVOID), ("InheritHandle", BOOL),]

class STARTUPINFO(STRUCT):
    _fields_ = [("cb",DWORD), ("lpReserved",LPWSTR), ("lpDesktop",LPWSTR), ("lpTitle",LPWSTR), ("dwX",DWORD), ("dwY",DWORD), ("dwXSize",DWORD), ("dwYSize",DWORD), ("dwXCountChars",DWORD), ("dwYCountChars",DWORD), ("dwFillAttribute",DWORD), ("dwFlags",DWORD), ("wShowWindow",WORD), ("cbReserved2",WORD), ("lpReserved2",POINTER(BYTE)), ("hStdInput",HANDLE), ("hStdOutput",HANDLE), ("hStdError",HANDLE),]

class PROCESS_INFORMATION(STRUCT):
    _fields_ = [("hProcess",HANDLE), ("hThread",HANDLE), ("dwProcessId",DWORD), ("dwThreadId",DWORD),]

class COORD(STRUCT):
    _fields_ = [("X", SHORT),("Y", SHORT)]

class SMALL_RECT(STRUCT):
    _fields_ = [('Left',SHORT),('Top',SHORT),('Right',SHORT),('Bottom',SHORT)]

class CONSOLE_SCREEN_BUFFER_INFO(STRUCT):
    _fields_ = [('dwSize', COORD), ('dwCursorPosition',COORD),('wAttributes',WORD),('srWindow',SMALL_RECT),('dwMaximumWindowSize',COORD)]

class CHAR_UNION(UNION): _fields_ = [('UnicodeChar',WCHAR),('AsciiChar',CHAR)]

class KEY_EVENT_RECORD(STRUCT):
    _fields_ = [('bKeyDown',BOOL),('wRepeatCount',WORD),('wVirtualKeyCode',WORD),('wVirtualScanCode',WORD),('uChar',CHAR_UNION),('dwControlKeyState',DWORD)]

class MOUSE_EVENT_RECORD(Structure):
    _fields_ = [('dwMousePosition',COORD),('dwButtonState',DWORD),('dwControlKeyState',DWORD),('dwEventFlags',DWORD)]

class WINDOW_BUFFER_SIZE_RECORD(Structure): _fields_ = [('dwSize', COORD)]

class MENU_EVENT_RECORD(Structure): _fields_ = [('dwCommandId', UINT)]

class FOCUS_EVENT_RECORD(Structure): _fields_ = [('bSetFocus', BOOL)]

class EVENT_UNION(UNION):
    _fields_ = ('KeyEvent',KEY_EVENT_RECORD),('MouseEvent',MOUSE_EVENT_RECORD),('WindowBufferSizeEvent',WINDOW_BUFFER_SIZE_RECORD),('MenuEvent',MENU_EVENT_RECORD),('FocusEvent',FOCUS_EVENT_RECORD)

class INPUT_RECORD(STRUCT): _fields_ = [('EventType',WORD), ('Event',EVENT_UNION)]

#========================================================


#==win32api proc.signatures===================
_LocalFree = windll.kernel32.LocalFree
_LocalFree.argtypes = [HLOCAL]
_LocalFree.restype = HLOCAL

_FormatMessage = windll.kernel32.FormatMessageW
_FormatMessage.argtypes = [DWORD,
                           LPVOID,
                           DWORD,
                           DWORD,
                           LPWSTR,
                           DWORD,
                           LPVOID]
_FormatMessage.restype = DWORD

_SHGetFolderPath = windll.shell32.SHGetFolderPathW
_SHGetFolderPath.argtypes = [HWND,
                             c_int,
                             HANDLE,
                             DWORD,
                             LPWSTR]
_SHGetFolderPath.restype = HRESULT

_GetLastError = windll.kernel32.GetLastError
_GetLastError.argtypes = []
_GetLastError.restype = DWORD

_GetExitCodeProcess = windll.kernel32.GetExitCodeProcess
_GetExitCodeProcess.argtypes = [HANDLE,
                                POINTER(DWORD)]
_GetExitCodeProcess.restype = BOOL

_PostThreadMessage = windll.user32.PostThreadMessageW
_PostThreadMessage.argtypes = [DWORD,
                               UINT,
                               WPARAM,
                               LPARAM]
_PostThreadMessage.restype = BOOL

_GetMessage = windll.user32.GetMessageW
_GetMessage.argtypes = [POINTER(MSG),
                        HWND,
                        UINT,
                        UINT]
_GetMessage.restype = BOOL

_GetConsoleWindow = windll.kernel32.GetConsoleWindow
_GetConsoleWindow.argtypes = []
_GetConsoleWindow.restype = int

_GetWindowThreadProcessId = windll.user32.GetWindowThreadProcessId
_GetWindowThreadProcessId.argtypes = [HWND,
                                      POINTER(DWORD)]
_GetWindowThreadProcessId.restype = DWORD

_GetConsoleProcessList = windll.kernel32.GetConsoleProcessList
_GetConsoleProcessList.argtypes = [POINTER(DWORD),
                                   DWORD]
_GetConsoleProcessList.restype = DWORD

_GetCurrentProcessId = windll.kernel32.GetCurrentProcessId
_GetCurrentProcessId.argtypes = []
_GetCurrentProcessId.restype = DWORD

_GetCurrentThreadId = windll.kernel32.GetCurrentThreadId
_GetCurrentThreadId.argtypes = []
_GetCurrentThreadId.restype = DWORD

_GetACP = windll.kernel32.GetACP
_GetACP.argtypes = []
_GetACP.restype = UINT

_GetStartupInfo = windll.kernel32.GetStartupInfoW
_GetStartupInfo.argtypes = [POINTER(STARTUPINFO)]
_GetStartupInfo.restype = None

_CreateProcess = windll.kernel32.CreateProcessW
_CreateProcess.argtypes = [LPCWSTR,
                           LPWSTR,
                           POINTER(SECURITY_ATTRIBUTES),
                           POINTER(SECURITY_ATTRIBUTES),
                           BOOL,
                           DWORD,
                           LPVOID,
                           LPCWSTR,
                           POINTER(STARTUPINFO),
                           POINTER(PROCESS_INFORMATION)]
_CreateProcess.restype = BOOL

_OpenProcess = windll.kernel32.OpenProcess
_OpenProcess.argtypes = [DWORD,
                         BOOL,
                         DWORD]
_OpenProcess.restype = HANDLE

_TerminateProcess = windll.kernel32.TerminateProcess
_TerminateProcess.argtypes = [HANDLE,
                              UINT]
_TerminateProcess.restype = BOOL

_CreateFile = windll.kernel32.CreateFileW
_CreateFile.argtypes = [LPCWSTR,
                        DWORD,
                        DWORD,
                        POINTER(SECURITY_ATTRIBUTES),
                        DWORD,
                        DWORD,
                        HANDLE]
_CreateFile.restype = HANDLE

_GetStdHandle = windll.kernel32.GetStdHandle
_GetStdHandle.argtypes = [DWORD]
_GetStdHandle.restype = HANDLE

_AttachConsole = windll.kernel32.AttachConsole
_AttachConsole.argtypes = [DWORD]
_AttachConsole.restype = BOOL

_FreeConsole = windll.kernel32.FreeConsole
_FreeConsole.argtypes = []
_FreeConsole.restype = BOOL

_AllocConsole = windll.kernel32.AllocConsole
_AllocConsole.argtypes = []
_AllocConsole.restype = BOOL

_GetConsoleScreenBufferInfo = windll.kernel32.GetConsoleScreenBufferInfo
_GetConsoleScreenBufferInfo.argtypes = [HANDLE,
                                        POINTER(CONSOLE_SCREEN_BUFFER_INFO)]
_GetConsoleScreenBufferInfo.restype = BOOL

_FillConsoleOutputCharacter = windll.kernel32.FillConsoleOutputCharacterW
_FillConsoleOutputCharacter.argtypes = [HANDLE,
                                        WCHAR,
                                        DWORD,
                                        COORD,
                                        POINTER(DWORD)]
_FillConsoleOutputCharacter.restype = BOOL

_ReadConsoleOutputCharacter = windll.kernel32.ReadConsoleOutputCharacterW
_ReadConsoleOutputCharacter.argtypes = [HANDLE,
                                        POINTER(WCHAR),
                                        DWORD,
                                        COORD,
                                        POINTER(DWORD)]
_ReadConsoleOutputCharacter.restype = BOOL

_SetConsoleCursorPosition = windll.kernel32.SetConsoleCursorPosition
_SetConsoleCursorPosition.argtypes = [HANDLE,
                                      COORD]
_SetConsoleCursorPosition.restype = BOOL

_SetConsoleScreenBufferSize = windll.kernel32.SetConsoleScreenBufferSize
_SetConsoleScreenBufferSize.argtypes = [HANDLE,
                                        COORD]
_SetConsoleScreenBufferSize.restype = BOOL

_SetConsoleWindowInfo = windll.kernel32.SetConsoleWindowInfo
_SetConsoleWindowInfo.argtypes = [HANDLE,
                                  BOOL,
                                  POINTER(SMALL_RECT)]
_SetConsoleWindowInfo.restype = BOOL

_WriteConsoleInput = windll.kernel32.WriteConsoleInputW
_WriteConsoleInput.argtypes = [HANDLE,
                               POINTER(INPUT_RECORD),
                               DWORD,
                               POINTER(DWORD)]
_WriteConsoleInput.restype = BOOL

_PeekConsoleInput = windll.kernel32.PeekConsoleInputW
_PeekConsoleInput.argtypes = [HANDLE,
                              POINTER(INPUT_RECORD),
                              DWORD,
                              POINTER(DWORD)]
_PeekConsoleInput.restype = BOOL

_SetConsoleCtrlHandler = windll.kernel32.SetConsoleCtrlHandler
_SetConsoleCtrlHandler.argtypes = [LPVOID,  #TODO: this should be replaced with PHANDLER_ROUTINE
                                   BOOL]
_SetConsoleCtrlHandler.restype = BOOL

_GenerateConsoleCtrlEvent = windll.kernel32.GenerateConsoleCtrlEvent
_GenerateConsoleCtrlEvent.argtypes = [DWORD,
                                      DWORD]
_GenerateConsoleCtrlEvent.restype = BOOL

_ShowWindow = windll.user32.ShowWindow
_ShowWindow.argtypes = [HWND,
                        c_int]
_ShowWindow.restype = BOOL

_SetConsoleOutputCP = windll.kernel32.SetConsoleOutputCP
_SetConsoleOutputCP.argtypes = [UINT]
_SetConsoleOutputCP.restype = BOOL

_GetConsoleMode = windll.kernel32.GetConsoleMode
_GetConsoleMode.argtypes = [HANDLE,
                            POINTER(DWORD)]
_GetConsoleMode.restype = BOOL

_SetConsoleMode = windll.kernel32.SetConsoleMode
_SetConsoleMode.argtypes = [HANDLE,
                            DWORD]
_SetConsoleMode.restype = BOOL

_GetConsoleOutputCP = windll.kernel32.GetConsoleOutputCP
_GetConsoleOutputCP.argtypes = []
_GetConsoleOutputCP.restype = UINT

_GetOEMCP = windll.kernel32.GetOEMCP
_GetOEMCP.argtypes = []
_GetOEMCP.restype = UINT

_OpenThread = windll.kernel32.OpenThread
_OpenThread.argtypes = [DWORD,
                        BOOL,
                        DWORD]
_OpenThread.restype = HANDLE

_SuspendThread = windll.kernel32.SuspendThread
_SuspendThread.argtypes = [HANDLE]
_SuspendThread.restype = DWORD

_ResumeThread = windll.kernel32.ResumeThread
_ResumeThread.argtypes = [HANDLE]
_ResumeThread.restype = DWORD

try:
    _CreateSymbolicLink = windll.kernel32.CreateSymbolicLinkW
    _CreateSymbolicLink.argtypes = [LPWSTR,
                                    LPWSTR,
                                    DWORD]
    _CreateSymbolicLink.restype = BOOL
except:
    _CreateSymbolicLink = None


#==================================================



#==win32api python wrappers=================================
def GetLastError():
    return _GetLastError()

def FormatMessage(winerrno):
    flags = FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS
    message = create_unicode_buffer(4096)
    n = _FormatMessage(flags, None, winerrno, 0, message, 1024, None)
    if not n:
        raise Exception("FormatMessage(errno={0}) failed : error {1}\n".format(winerrno, GetLastError()))
    return unicode(message.value)

class WinError(WindowsError):
    def __init__(self, winerrno):
        super(WinError, self).__init__(winerrno)
        self.errno = winerrno
        self.message = FormatMessage(winerrno)
    def __unicode__(self):
        return u"WinError #{0} : {1}".format(self.errno, self.message)
    def __str__(self):
        return self.__unicode__()
    def __repr__(self):
        return self.__unicode__()

def LocalFree(mem):
    _LocalFree(mem)

def SHGetFolderPath(folder_id, access_token=None):
    path_buf = create_unicode_buffer(MAX_PATH)
    hresult = _SHGetFolderPath(None, folder_id, access_token, 0, path_buf)
    if hresult:
        raise WinError(hresult)
    return unicode(path_buf.value)

def SHGetSpecialFolderPath(owner_hdl, folder_id, create=False):
    """Not supported. Use SHGetFolderPath"""
    if owner_hdl or create:
        raise NotImplemented("Only arguments owner_hdl=0 and create=False is supported.")
    return SHGetFolderPath(folder_id)

def GetExitCodeProcess(hProcess):
    res = DWORD(0)
    if not _GetExitCodeProcess(hProcess, res):
        raise WinError(GetLastError())
    return res.value

def PostThreadMessage(tid, idMessage, wParam, lParam):
    return _PostThreadMessage(tid, idMessage, wParam, lParam)

def GetMessage(hwnd, min , max):
    msg = MSG()
    res = _GetMessage(msg, hwnd, min, max)
    return (res, msg)

def GetConsoleWindow():
    return _GetConsoleWindow()

def GetWindowThreadProcessId(hwnd):
    pid = DWORD()
    tid = _GetWindowThreadProcessId(hwnd, pid)
    return int(tid), int(pid.value)

def GetConsoleProcessList():
    LPDWORD = POINTER(DWORD)
    def c_buf(elem_type, buf_size):
        return (elem_type * buf_size)()
    def _get_console_pid_list(buf_size):
        pid_buf = c_buf(DWORD, buf_size)
        required_buf_size = _GetConsoleProcessList(LPDWORD(pid_buf), buf_size)
        return required_buf_size, pid_buf
    buf_size = 10
    while True:
        required_buf_size, buf = _get_console_pid_list(buf_size)
        if not required_buf_size:
            raise WinError(GetLastError())
        elif required_buf_size <= buf_size:
            return [i for i in buf[:required_buf_size]]
        elif required_buf_size > 1000:
            raise WinError(ERROR_DEVICE_NO_RESOURCES)
        else:
            del buf
            buf_size = required_buf_size + 5    #just in case if new console processes just burst in
            continue

def GetCurrentProcessId():
    return _GetCurrentProcessId()

def GetCurrentThreadId():
    return _GetCurrentThreadId()

def GetACP():
    return _GetACP()

def GetStartupInfo():
    si = STARTUPINFO()
    _GetStartupInfo(si)
    return si

def CreateProcess(appName, commandLine ,
                  processAttributes , threadAttributes , bInheritHandles , dwCreationFlags ,
                  newEnvironment , currentDirectory , startupinfo):
    processinfo = PROCESS_INFORMATION()
    if not _CreateProcess(appName,commandLine,processAttributes,threadAttributes,bInheritHandles,dwCreationFlags,newEnvironment,currentDirectory,startupinfo,processinfo):
        raise WinError(GetLastError())
    return (processinfo.hProcess, processinfo.hThread, processinfo.dwProcessId, processinfo.dwThreadId)

def OpenProcess(reqdAccess, bInherit , pid):
    hdl = _OpenProcess(reqdAccess, bInherit, pid)
    if not hdl:
        raise WinError(GetLastError())
    return hdl

def TerminateProcess(hdl, exitCode=0):
    if not _TerminateProcess(hdl, exitCode):
        raise WinError(GetLastError())

def CreateFile(fileName, desiredAccess , shareMode , attributes ,
               CreationDisposition , flagsAndAttributes , hTemplateFile):
    return _CreateFile(fileName,desiredAccess,shareMode,attributes,CreationDisposition,flagsAndAttributes,hTemplateFile)

def GetStdHandle(stdHandleId):
    return _GetStdHandle(stdHandleId)

def AttachConsole(pid):
    return _AttachConsole(pid)

def FreeConsole():
    return _FreeConsole()

def AllocConsole():
    return _AllocConsole()

def GetConsoleScreenBufferInfo(hConsoleOut):
    buf_info = CONSOLE_SCREEN_BUFFER_INFO()
    if not _GetConsoleScreenBufferInfo(hConsoleOut, buf_info):
        raise WinError(GetLastError())
    return buf_info

def FillConsoleOutputCharacter(hConsoleOut, char, len, pos):
    """Returns number of chars written"""
    nCharsWritten = DWORD()
    if not _FillConsoleOutputCharacter(hConsoleOut, char, len, pos, nCharsWritten):
        raise WinError(GetLastError())
    return int(nCharsWritten.value)

def ReadConsoleOutputCharacter(hConsoleOut, len, pos):
    """Returns string of len (at max) chars read from console at pos"""
    LPWCHAR = POINTER(WCHAR)
    def c_buf(elem_type, buf_size):
        return (elem_type * buf_size)()
    wchar_buf = c_buf(WCHAR, len+5)
    nCharsRead = DWORD()
    if not _ReadConsoleOutputCharacter(hConsoleOut, LPWCHAR(wchar_buf), len, pos, nCharsRead):
        raise WinError(GetLastError())
    return u'' + wchar_buf[0:int(nCharsRead.value)]

def SetConsoleCursorPosition(hConsoleOut, pos):
    return _SetConsoleCursorPosition(hConsoleOut, pos)

def SetConsoleScreenBufferSize(hConsoleOut, size):
    return _SetConsoleScreenBufferSize(hConsoleOut, size)

def SetConsoleWindowInfo(hConsoleOut, abs, rect):
    return _SetConsoleWindowInfo(hConsoleOut, abs, rect)

def WriteConsoleInput(hConsoleIn, records):
    """records - python list of C structures"""
    LPINPUT_RECORD = POINTER(INPUT_RECORD)
    def c_buf(elem_type, buf_size):
        return (elem_type * buf_size)()
    rec_buf = type_cast(c_buf(INPUT_RECORD, len(records)), LPINPUT_RECORD)
    for i, rec in enumerate(records):
        rec_buf[i].copy_from(rec)
    nRecsWritten = DWORD()
    if not _WriteConsoleInput(hConsoleIn, rec_buf, len(records), nRecsWritten):
        raise WinError(GetLastError())
    return nRecsWritten.value

def PeekConsoleInput(hConsoleIn, len):
    LPINPUT_RECORD = POINTER(INPUT_RECORD)
    def c_buf(elem_type, buf_size):
        return (elem_type * buf_size)()
    rec_buf = type_cast(c_buf(INPUT_RECORD, len), LPINPUT_RECORD)
    nRecsRead = DWORD()
    if not _PeekConsoleInput(hConsoleIn, rec_buf, len, nRecsRead):
        raise WinError(GetLastError())
    res = [INPUT_RECORD().copy_from(rec_buf[i]) for i in range(int(nRecsRead.value))]
    return tuple(res)

def SetConsoleCtrlHandler(hdlr_proc, add):
    return _SetConsoleCtrlHandler(hdlr_proc, add)

def GenerateConsoleCtrlEvent(ctrlEvent, pgid):
    if not _GenerateConsoleCtrlEvent(ctrlEvent, pgid):
        raise WinError(GetLastError())

def ShowWindow(hwnd, cmdShow):
    return _ShowWindow(hwnd, cmdShow)

def SetConsoleOutputCP(codePage):
    return _SetConsoleOutputCP(codePage)

def GetConsoleMode(hConsole):
    mode = DWORD()
    if not _GetConsoleMode(hConsole, mode):
        raise WinError(GetLastError())
    return mode.value

def SetConsoleMode(hConsole, mode):
    return _SetConsoleMode(hConsole, mode)

def GetConsoleOutputCP():
    return _GetConsoleOutputCP()

def GetOEMCP():
    return _GetOEMCP()

def OpenThread(reqdAccess, bInherit , pid):
    return _OpenThread(reqdAccess, bInherit, tid)

def SuspendThread(hThread):
    return _SuspendThread(hThread)

def ResumeThread(hThread):
    return _ResumeThread(hThread)

def CreateSymbolicLink(src_path, target_path, flags=0):
    if not _CreateSymbolicLink:
        raise WinError(ERROR_NOT_SUPPORTED)
    return _CreateSymbolicLink(src_path, target_path, flags)

