# About

 - A simple Python module for accessing many of the Windows API functions from Python code.
 - For more robust and feature-full alternative check the pywin32 python extentions package: https://github.com/mhammond/pywin32

